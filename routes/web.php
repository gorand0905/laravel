<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
	echo "hello";
	echo config('app.locale');
    return view('welcome');
});
// Route::get('/', ['as'=>'home', function () {
// 	echo "hello";
// 	echo config('app.locale');
//     return ;
// }]);

Route::get('/about', 'FirstController@show');

Route::get('/articles', ['uses' => 'Core@getArticles', 'as' => 'articles']);

Route::get('/article/{id}', ['uses' => 'Core@getArticle', 'as' => 'article']);

Route::resource('/pages', 'CoreResource');

// Route::post('/pages', 'CoreResource@store');


