<?php

/**
 * First Controller 
 */
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;


class FirstController extends Controller{
	
	// function __construct(argument)
	// {
	// 	# code...
	// }

	public function show(){
		echo __METHOD__;
	}
}
